import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="row">
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers/create">Add a Manufacturer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/models">Models</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/models/new">Add a Model</NavLink>
                </li>
              </ul>
            </div>
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales">Salesrecords</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customer">Customers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customer/new">Add a Customer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople">Salespersons</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/new">Add a Salesperson</NavLink>
                </li>
              </ul>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <ul className="navbar-nav">
                <li className='nav-item'>
                  <NavLink className="nav-link" to="/technicians">Technicians</NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="nav-link" to="/technicians/create">Add a Technician</NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="nav-link" to="/automobiles/new">Add a Automobile</NavLink>
                </li>
              </ul>
            </div>
            <div className="col">
              <ul className="navbar-nav">
                <li className='nav-item'>
                  <NavLink className="nav-link" to="/appointments/">Appointments</NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="nav-link" to="/appointments/create">Create a Appointment</NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="nav-link" to="servicehistory/">Service History</NavLink>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
