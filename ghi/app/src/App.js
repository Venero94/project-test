import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './Sales/CustomerForm';
import SalesPersonForm from './Sales/SalesPersonForm';
import CustomerList from './Sales/CustomerList';
import SalesPersonList from './Sales/SalesPersonlist';
import CreateTechnician from './Service/CreateTechnician';
import ListTechnicians from './Service/ListTechnicians';
import VehicleForm from './Sales/VehicleForm';
import VehicleList from './Sales/VehicleList';
import ListAppointments from './Service/ListAppointments';
import CreateAppointment from './Service/CreateAppointment';
import ServiceHistory from './Service/ServiceHistory';
import ListManufacturers from './Service/ListManufacturers';
import AutoForm from './Sales/AutoForm';
import CreateManufacturer from './Service/CreateManufacturer';
import ListAutomobiles from './Service/ListAutomobiles';
import SaleRecordList from './Sales/SaleRecordList';
import SaleForm from './Sales/SaleForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customer">
            <Route path ="new" element={<CustomerForm/>}/>
            <Route path="" element={<CustomerList/>}/>
          </Route>
          <Route path="salespeople">
            <Route path="new" element={<SalesPersonForm/>}/>
            <Route path="" element={<SalesPersonList/>}/>
          </Route>
          <Route path="models">
            <Route path="new" element={<VehicleForm/>}/>
            <Route path="" element={<VehicleList/>}/>
          </Route>
          <Route path="technicians">
            <Route path="" element={<ListTechnicians />} />
            <Route path="create" element={<CreateTechnician/>} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<ListAppointments />} />
            <Route path="create" element={<CreateAppointment />} />
          </Route>
          <Route path="servicehistory" element={<ServiceHistory />} />
          <Route path="manufacturers">
            <Route path="" element={<ListManufacturers />} />
            <Route path="create" element={<CreateManufacturer />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<ListAutomobiles />} />
            <Route path="new" element={<AutoForm/>}/>
          </Route>
          <Route path="sales">
            <Route path="" element={<SaleRecordList/>}/>
            <Route path="new" element={<SaleForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
