import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function SalesPersonList(){
  const [sale, setSale]=useState([])
  const [salesPerson, setSalesPerson]=useState([])

  const fetchData=async()=>{
    const response = await fetch('http://localhost:8090/api/sales/')

//
    if (response.ok){
      const data= await response.json()
      setSale(data.sale)
    }


    const personResponse=await fetch("http://localhost:8090/api/salespeople/")
    if (personResponse.ok){
      const personData=await personResponse.json()
      setSalesPerson(personData.person)
    }
  }
    useEffect(()=>{
      fetchData()
    },[])

    return (
      <>
      <div className="d-flex justify-content-end">
        <NavLink className="nav-link" to="/sales/new">
          <button type="button" className="btn btn-success">Create a Sale</button>
        </NavLink>
      </div>
        <table className="table table-striped">
            <thead>
              <tr>
                <th>Sales Person</th>
                <th>Employee id</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {sale?.map(sales => {
                return (
                  <tr key={sales.id}>
                    <td>{sales.sales_person.first_name}</td>
                    <td>{sales.sales_person.employee_number}</td>
                    <td>{sales.customer.first_name}</td>
                    <td>{sales.automobile.vin}</td>
                    <td>{sales.price}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
      </>
    )
  }

  export default SalesPersonList;


// dont need seaerch bar here
  // function SalesPersonList() {
//   const [sale, setSale] = useState([]);
//   const [salesPerson, setSalesPerson] = useState([]);
//   const [selectedSalesPerson, setSelectedSalesPerson] = useState("");

//   const fetchData = async () => {
//     const response = await fetch("http://localhost:8090/api/sales/");

//     if (response.ok) {
//       const data = await response.json();
//       setSale(data.sale);
//     }

//     const personResponse = await fetch("http://localhost:8090/api/salespeople/");
//     if (personResponse.ok) {
//       const personData = await personResponse.json();
//       setSalesPerson(personData.person);
//       setSelectedSalesPerson(personData.person[0]?.id);
//     }
//   };
//   useEffect(() => {
//     fetchData();
//   }, []);

//   const handleSalesPersonChange = (event) => {
//     setSelectedSalesPerson(event.target.value);
//   };

//   const filteredSale = sale.filter(
//     (s) => s.sales_person.id === selectedSalesPerson
//   );

//   return (
//     <>
//       <div className="d-flex justify-content-end">
//         <NavLink className="nav-link" to="/sales/new">
//           <button type="button" className="btn btn-success">
//             Create a Sale
//           </button>
//         </NavLink>
//       </div>
//       <div className="d-flex justify-content-end">
//         {/* <select
//           value={selectedSalesPerson}
//           onChange={handleSalesPersonChange}
//           className="form-select"
//           aria-label="Select Sales Person"
//         >
//           {salesPerson.map((sp) => (
//             <option key={sp.id} value={sp.id}>
//               {sp.first_name} {sp.last_name}
//             </option>
//           ))}
//         </select> */}
//       </div>
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Sales Person</th>
//             <th>Employee id</th>
//             <th>Customer</th>
//             <th>VIN</th>
//             <th>Price</th>
//           </tr>
//         </thead>
//         <tbody>
//           {filteredSale.map((sales) => {
//             return (
//               <tr key={sales.id}>
//                 <td>{sales.sales_person.first_name}</td>
//                 <td>{sales.sales_person.employee_number}</td>
//                 <td>{sales.customer.first_name}</td>
//                 <td>{sales.automobile.vin}</td>
//                 <td>{sales.price}</td>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>
//     </>
//   );
// }

// export default SalesPersonList;
