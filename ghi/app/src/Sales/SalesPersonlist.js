import React, {useState, useEffect} from "react";
import { NavLink } from "react-router-dom";

function SalesPersonList(){
  const [salesPerson, setSalesPerson]=useState([])

  const fetchData=async()=>{
    const response = await fetch('http://localhost:8090/api/salespeople/')

    if (response.ok){
      const data= await response.json()

      setSalesPerson(data)
    }
  }
    useEffect(()=>{
      fetchData()
    },[])

    return (
      <>
      <div className="d-flex justify-content-end">
        <NavLink className="nav-link" to="/salespeople/new">
          <button type="button" className="btn btn-success">Create a Sales Person</button>
        </NavLink>
      </div>
        <table className="table table-striped">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Employee Number</th>
              </tr>
            </thead>
            <tbody>
              {salesPerson.person?.map(person => {
                return (
                  <tr key={person.id}>
                    <td>{person.first_name}</td>
                    <td>{person.last_name}</td>
                    <td>{person.employee_number}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
      </>
    )
  }

  export default SalesPersonList;
