import React, {useState, useEffect} from "react";
import { NavLink } from "react-router-dom";

function VehicleList(){
    const [models, setModels]=useState([])

    const fetchData=async()=>{
      const response = await fetch('http://localhost:8100/api/models/')
      if (response.ok){
        const data= await response.json()
        setModels(data.models)
      }
    }
      useEffect(()=>{
        fetchData()
      },[])

      return (
        <>
        <div className="d-flex justify-content-end">
          <NavLink className="nav-link" to="/models/new">
            <button type="button" className="btn btn-success">Create a New Model</button>
          </NavLink>
        </div>
          <table className="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>manufacturer</th>
                  <th>Picture</th>
                </tr>
              </thead>
              <tbody>
                {models?.map(model => {
                  return (
                    <tr key={model.id}>
                      <td>{model.name}</td>
                      <td>{model.manufacturer.name}</td>
                      <td className="w-25 rounded">
                        <img src={model.picture_url} alt={model.manufacturer.name} className="img-fluid"/>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
        </>
      )
    }

    export default VehicleList;
