import React, {useEffect, useState} from "react";

function SaleForm(){
  const [customer, setCustomer]=useState('')
  const [auto, setAuto]=useState('')
  const [salesPerson, setSalesPerson]=useState('')
  const [price, setPrice]=useState('')
  const [customers, setCustomers]=useState([])
  const [autos, setAutos]=useState([])
  const [salesPersons, setSalesPersons]=useState([])

  const handleCustomerChange=(event)=>{
    const value= event.target.value
    setCustomer(value)
  }

  const handleAutoChange=(event)=>{
    const value= event.target.value
    setAuto(value)
  }

  const handleSalesChange=(event)=>{
    const value= event.target.value
    setSalesPerson(value)
  }
  const handlePriceChange=(event)=>{
    const value= event.target.value
    setPrice(value)
  }

  const handleSubmit= async (event)=>{
    event.preventDefault()
    const data={}
    data.price=price
    data.automobile=auto
    data.customer=customer
    data.sales_person=salesPerson



    const url="http://localhost:8090/api/sales/";
    const fetchConfig={
      method:"post",
      body:JSON.stringify(data),
      headers:{
        "Content-type": "application/json",
      }
    }
    const response=await fetch(url, fetchConfig)

    if (response.ok){
      const newSales=await response.json()

      setPrice('')
      setAuto('')
      setCustomer('')
      setSalesPerson('')

    }
  }

  const fetchData=async()=>{
    const autoUrl="http://localhost:8100/api/automobiles"
    const autoResponse=await fetch(autoUrl)
    if (autoResponse.ok){
      const autoData=await autoResponse.json()
      setAutos(autoData.autos)
    }

    const customerUrl="http://localhost:8090/api/customers/"
    const customerResponse=await fetch(customerUrl)
    if (customerResponse.ok){
      const customerData =await customerResponse.json()
      setCustomers(customerData.customers)
    }

    const salesUrl="http://localhost:8090/api/salespeople/"
    const salesResponse= await fetch(salesUrl)
    if (salesResponse.ok){
      const salesData=await salesResponse.json()
      setSalesPersons(salesData.person)
    }
  }
  useEffect(()=>{
    fetchData()
  }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Sale Record</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-form">
              <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="price" required type="text" name="price" id="price" className="form-control"/>
                <label htmlFor="price">Price</label>
              </div>
              <div className="mb-3">
                <select onChange={handleAutoChange} value={auto} placeholder="vin" name="vin" required id="vin" className="form-select">
                <option value=" " >VIN</option>
                {autos?.map( automobile=>{
                  return(
                    <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                  )
                })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleCustomerChange} value={customer} placeholder="customer" name="customer" required id="customer" className="form-select">
                <option value=" " >Customer</option>
                {customers?.map(customer =>{
                  return(
                    <option key={customer.id} value={customer.id}>{customer.first_name}</option>
                  )
                })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleSalesChange} value={salesPerson} placeholder="sales_person" name="sales_person" required id="sales_person" className="form-select">
                <option value=" " >Sales Person</option>
                {salesPersons?.map(sales_person =>{
                  return(
                    <option key={sales_person.id} value={sales_person.id}>{sales_person.employee_number}</option>
                  )
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default SaleForm
