import React, {useEffect, useState} from "react";

function AutoForm(){
  const [color, setColor]=useState('')
  const [year, setYear]=useState('')
  const [vin, setVin] =useState('')
  const [model, setModel]=useState('')
  const [models, setModels]=useState([])

  const handleColorChange=(event)=>{
    const value=event.target.value
    setColor(value)
  }

  const handleYearChange=(event)=>{
    const value=event.target.value
    setYear(value)
  }

  const handleVinChange=(event)=>{
    const value=event.target.value
    setVin(value)
  }
  const handleModelChange=(event)=>{
    const value=event.target.value
    setModel(value)
  }

  const handleSubmit=async(event)=>{
    event.preventDefault()
    const data={}
    data.color=color
    data.year=year
    data.vin=vin
    data.model_id=model


    const url='http://localhost:8100/api/automobiles/'
    const fetchConfig={
      method:"post",
      body:JSON.stringify(data),
      headers: {
        "Content-type":"application/json"
      }
    }
    const response=await fetch(url, fetchConfig)

    if (response.ok){
      const newAuto=await response.json()

      setColor('')
      setYear('')
      setVin('')
      setModel('')
    }
  }

  const fetchData=async()=>{
    const modelUrl= 'http://localhost:8100/api/models/'
    const response= await fetch(modelUrl)
    if (response.ok){
      const data=await response.json()
      setModels(data.models)
    }
  }
  useEffect(()=>{
    fetchData()
  },[])
    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New Automobile</h1>
          <form onSubmit={handleSubmit} id="create-vehicle-form">
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleYearChange} value={year} placeholder="year" required type="number" name="year" id="year" className="form-control"/>
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
              <select onChange={handleModelChange} value={model} placeholder="model" name="model" required id="model" className="form-select">
              <option value=" " >Choose a Model</option>
              {models?.map(model =>{
                return(
                  <option key={model.id} value={model.id}>{model.name}</option>
                )
              })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}

export default AutoForm
// import React, { useEffect, useState } from "react";

// function AutoForm() {
//   const [models, setModels] = useState([]);
//   const [color, setColor] = useState("");
//   const [year, setYear] = useState("");
//   const [vin, setVin] = useState("");
//   const [model, setModel] = useState("");

//   const fetchData = async () => {
//     const url = "http://localhost:8100/api/models/";
//     const response = await fetch(url);
//     if (response.ok) {
//       const data = await response.json();
//       setModels(data.models);
//     }
//   };

//   useEffect(() => {
//     fetchData();
//   }, []);

//   const handleSubmit = async (event) => {
//     event.preventDefault();
//     const data = {};

//     data.color = color;
//     data.year = year;
//     data.vin = vin;
//     data.model_id = model;

//     const automobileUrl = "http://localhost:8100/api/automobiles/";
//     const fetchOptions = {
//       method: "post",
//       body: JSON.stringify(data),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     };
//     const automobileResponse = await fetch(automobileUrl, fetchOptions);
//     if (automobileResponse.ok) {
//       setColor("");
//       setYear("");
//       setVin("");
//       setModel("");
//     }
//   };

//   const handleColorChange = (event) => {
//     setColor(event.target.value);
//   };
//   const handleYearChange = (event) => {
//     setYear(event.target.value);
//   };
//   const handleVinChange = (event) => {
//     setVin(event.target.value);
//   };
//   const handleModelChange = (event) => {
//     setModel(event.target.value);
//   };

//   return (
//     <div className="my-5 container">
//       <div className="card-body">
//         <form onSubmit={handleSubmit} id="create-automobile-form">
//           <h1 className="card-title"></h1>
//           <p className="mb-3">Please select a model for you automobile</p>

//           <div className="mb-3">
//             <select
//               onChange={handleModelChange}
//               value={model}
//               name="model"
//               id="model"
//               className="form-select"
//               required
//             >
//               <option value="">Choose a model</option>
//               {models.map((model) => {
//                 return (
//                   <option key={model.id} value={model.id}>
//                     {model.name}
//                   </option>
//                 );
//               })}
//             </select>
//           </div>
//           <div>
//             <div>
//               <div className="form-floating mb-3">
//                 <input
//                   onChange={handleColorChange}
//                   value={color}
//                   placeholder="Primary car color"
//                   required
//                   type="text"
//                   id="color"
//                   name="color"
//                   className="form-control"
//                 />
//                 <label htmlFor="color">Color</label>
//               </div>
//             </div>

//             <div>
//               <div className="form-floating mb-3">
//                 <input
//                   onChange={handleYearChange}
//                   value={year}
//                   placeholder="Year"
//                   required
//                   type="text"
//                   id="year"
//                   name="year"
//                   className="form-control"
//                 />
//                 <label htmlFor="name">Year of model</label>
//               </div>
//             </div>

//             <div>
//               <div className="form-floating mb-3">
//                 <input
//                   onChange={handleVinChange}
//                   value={vin}
//                   placeholder="vin number"
//                   required
//                   type="vin"
//                   id="vin"
//                   name="vin"
//                   className="form-control"
//                 />
//                 <label htmlFor="vin">vin Number</label>
//               </div>
//             </div>
//           </div>

//           <button className="btn btn-lg btn-primary">
//             Add Car To Inventory
//           </button>
//         </form>
//       </div>
//     </div>
//   );
// }

// export default AutoForm;
