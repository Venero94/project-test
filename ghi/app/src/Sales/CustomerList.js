import React, {useState, useEffect} from "react";
import { NavLink } from "react-router-dom";

function CustomerList(){
  const [customer, setCustomer]=useState([])

  useEffect(()=>{
    getCustomers()
  },[])

  async function getCustomers(){
    const response = await fetch('http://localhost:8090/api/customers/')
    if (response.ok){
      const data= await response.json()
      setCustomer(data)
    }
  }
    return (
      <>
      <div className="d-flex justify-content-end">
        <NavLink className="nav-link" to="/customer/new">
          <button type="button" className="btn btn-success">Create a Customer</button>
        </NavLink>
      </div>
        <table className="table table-striped">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Phone Number</th>
              </tr>
            </thead>
            <tbody>
              {customer.customers?.map(customer => {
                return (
                  <tr key={customer.id}>
                    <td>{customer.first_name}</td>
                    <td>{customer.last_name}</td>
                    <td>{customer.address}</td>
                    <td>{customer.phone_number}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
      </>
    )
  }

  export default CustomerList;
