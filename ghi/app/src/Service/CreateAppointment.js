import React, { useState, useEffect } from 'react';

function CreateAppointment(props) {
    const [vin, setVin] = useState('');
    const [customerName, setCustomerName] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [technicianId, setTechnicianId] = useState('');
    const [technicians, setTechnicians] = useState([]);

    async function handleSubmit(event) {
      event.preventDefault();
      const data = {
        vin: vin,
        customer_name: customerName,
        date:date,
        time:time,
        reason:reason,
        technician:technicianId,
      };

      // console.log('data->:', data)
      // console.log('JSON.stringify(data) ->:', JSON.stringify(data))
      const locationUrl = 'http://localhost:8080/api/appointments/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        await response.json();
        setVin('');
        setCustomerName('');
        setDate('');
        setTime('');
        setReason('');
        setTechnicianId('');


        const successMessage = document.createElement('div');
        successMessage.innerHTML = 'Submittion Successful! Jumping to Appointments List now';
        successMessage.style.color = 'red';
        document.getElementById('create-appointment-form').appendChild(successMessage);
        setTimeout(() => {
          window.location.href = "http://localhost:3000/appointments/";
        }, 3000);
      }
    }

    function handleChangeVin(event) {
      const value = event.target.value;
      setVin(value);
    }
    function handleChangeCustomerName(event) {
      const value = event.target.value;
      setCustomerName(value);
    }
    function handleChangeDate(event) {
      const value = event.target.value;
      setDate(value);
    }
    function handleChangeTime(event) {
      const value = event.target.value;
      setTime(value);
    }
    function handleChangeReason(event) {
      const value = event.target.value;
      setReason(value);
    }
    function handleChangeTechnicianId(event) {
      const value = event.target.value;
      setTechnicianId(value);
    }
    const fetchData=async()=>{
      const technicianUrl= 'http://localhost:8080/api/technicians/'
      const response= await fetch(technicianUrl)
      if (response.ok){
        const data=await response.json()
        setTechnicians(data.technicians)
      }
    }
    useEffect(()=>{
      fetchData()
    },[])

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow-lg p-4 mt-4 border border-success">
            <h1>Create a new Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form"  className="text-center" >
              <div className="form-floating mb-3">
                <input value={customerName} onChange={handleChangeCustomerName} placeholder="customer_name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                <label htmlFor="customer_name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={vin} onChange={handleChangeVin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input value={date} onChange={handleChangeDate} placeholder="date" required type="text" name="date" id="date" className="form-control" />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input value={time} onChange={handleChangeTime} placeholder="time" required type="text" name="time" id="time" className="form-control" />
                <label htmlFor="time">Time</label>
              </div>
              <div className="form-floating mb-3">
                <input value={reason} onChange={handleChangeReason} placeholder="reason" required type="text" name="reason" id="time" className="form-control" />
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="mb-3">
                <select value={technicianId} onChange={handleChangeTechnicianId} required name="technician_id" id="technician_id" className="form-select">
                  <option value="">Choose a technician</option>
                  {technicians.map(technician => {
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.name_first + " " + technician.name_last}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-success">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

  export default CreateAppointment;
