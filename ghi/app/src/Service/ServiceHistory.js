import React, { useEffect, useState } from 'react';

function ServiceHistory() {
    const [vin, setVin] = useState("")
    const [targetedAppointments, setTargetedAppointments] = useState([])
    const [data, setData] = useState([])
    const [searchClicked, setSearchClicked] = useState(false)

    useEffect(()=> {
        getAppointments();
    },[])

    async function getAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const data = await response.json();
            setData(data.appointments);
            if (vin === "") {
                setTargetedAppointments(data.appointments);
            } else {
                const targetedAppointments = await data.appointments.filter(
                    appointment => appointment.vin === vin
                );
                // const targetedAppointments2 = await targetedAppointments.filter(
                //     appointment => {return appointment.is_finished || !appointment.is_finished}
                // );
                setTargetedAppointments(targetedAppointments);
            }
        }
    }

    useEffect(() => {
        getAppointments();
    }, [vin]);

    useEffect(() => {
        if (searchClicked) {
            getAppointments();
            setSearchClicked(false);
        }
    }, [searchClicked]);

    return (
        <>
            <h1>Service history</h1>
            <div className="d-flex justify-content-end">
                <input onChange={e => setVin(e.target.value)} type="search" id="search" name="search" placeholder="Search by VIN"/>
                <button onClick={() => setSearchClicked(true)} htmlFor="search" type="submit" className="btn btn-success">Search</button>
            </div>
            <table className="table table-striped">
            <thead>
                <tr>
                <th>VIN</th>
                <th>Customer name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Finished?</th>
                <th>is VIP?</th>
                </tr>
            </thead>
            <tbody>
                {targetedAppointments?.map(appointment => {
                return (
                    <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{ appointment.customer_name }</td>
                    <td>{ appointment.date }</td>
                    <td>{ appointment.time }</td>
                    <td>{ appointment.technician.name_first + " " + appointment.technician.name_last }</td>
                    <td>{ appointment.reason }</td>
                    {appointment.is_finished && <td>Y</td>}
                    {!appointment.is_finished && <td>N</td>}
                    {appointment.vip && <td>Y</td>}
                    {!appointment.vip && <td>N</td>}
                    </tr>
                );
                })}
            </tbody>
            </table>
        </>
    );
}

export default ServiceHistory;
