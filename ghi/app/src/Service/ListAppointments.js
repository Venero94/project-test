import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';


function ListAppointments() {
    const [appointments, setAppointment] = useState([])
    const [unfinishedAppointments, setUnfinishedAppointment] = useState([])
    useEffect(()=> {
        getAppointments();
    },[])
    async function getAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const data = await response.json();
            const unfinishedAppointments = data.appointments.filter(
                appointment => { return appointment.is_finished === false }
                );
            setAppointment(data);
            setUnfinishedAppointment(unfinishedAppointments)
        }
    }

    async function deleteAppointment(id) {
      await fetch(`http://localhost:8080/api/appointments/${id}/`, {
        method: "delete",
        headers: {
          "Content-Type": "application/json"
        },
      });
      if (Array.isArray(appointments)) {
        const updatedAppointments = appointments.filter(appointment => appointment.id !== id);
        setAppointment(updatedAppointments);
      }
      if (Array.isArray(unfinishedAppointments)) {
        const updatedUnfinishedAppointments = unfinishedAppointments.filter(appointment => appointment.id !== id);
        setUnfinishedAppointment(updatedUnfinishedAppointments);
      }
    }


    async function updateAppointment(id) {
      await fetch(`http://localhost:8080/api/appointments/${id}/`, {
        method: "put",
        body: JSON.stringify({
          is_finished: true,
        }),
        headers: {
          "Content-Type": "application/json"
        },
      });

      const response = await fetch('http://localhost:8080/api/appointments/')
      if (response.ok) {
        const data = await response.json();
        const unfinishedAppointments = data.appointments.filter(
          appointment => { return appointment.is_finished === false }
        );
        setAppointment(data);
        setUnfinishedAppointment(unfinishedAppointments)
      }
    }

  return (
    <>
        <h1>Service appointments</h1>
        <div className="d-flex justify-content-end">
          <NavLink className="nav-link" to="/appointments/create">
            <button type="button" className="btn btn-success">Add appointment</button>
          </NavLink>
        </div>
        <table className="table table-striped">
          <thead>
              <tr>
              <th>Customer name</th>
              <th>VIP</th>
              <th>VIN</th>
              <th>Date</th>
              <th>Time</th>
              <th>Reason</th>
              <th>Technician</th>
              <th>Finished?</th>
              <th></th>
              <th>Cancellation</th>
              </tr>
          </thead>
          <tbody>
              {unfinishedAppointments?.map(appointment => {
              return (
                  <tr key={appointment.id}>
                  <td>{ appointment.customer_name }</td>
                  {appointment.vip && <td>Y</td>}
                  {!appointment.vip && <td>N</td>}
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.date }</td>
                  <td>{ appointment.time }</td>
                  <td>{ appointment.reason }</td>
                  <td>{ appointment.technician.name_first + " " + appointment.technician.name_last }</td>
                  {appointment.is_finished && <td>Y</td>}
                  {!appointment.is_finished && <td>N</td>}
                  <td><button onClick={() => updateAppointment(appointment.id)} type="button" className="btn btn-primary">Finish</button></td>
                  <td><button onClick={() => deleteAppointment(appointment.id)} type="button" className="btn btn-danger">Cancel</button></td>
                  </tr>
              );
              })}
          </tbody>
        </table>
    </>
  );
}

export default ListAppointments;
