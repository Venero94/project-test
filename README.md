![alt text](Project-Beta-Excalidraw.png)


# CarCar

---

Team:

* Robert Venegas - Sales microservice
* Bran Tai - Service microservice

---

## How to Run this Project

To start the application, please follow these steps in order：

1. Fork and clone the repository at https://gitlab.com/Venero94/project-beta .
2. Click the blue Clone button with the dropdown arrow and either copy the URL or click the copy button next to the ‘Clone with HTTPS’ option.
3. Open a terminal window in VSCode or a separate terminal and navigate to the desired directory using one of two options: (1) navigate to the desired directory using “cd <‘desired directory’>” or (2) create a directory using “mkdir <‘desired directory’>” that the app will be stored in and navigate to it using “cd <‘desired directory’>”.
4. Once navigated to the correct directory, execute “git clone” with the copied repository link, i.e., “git clone https://gitlab.com/Venero94/project-beta”.
5. Type in “code .” to open the app in a VSCode window.
6. This app requires Docker - Please perform the following steps:
    1. Open Docker Desktop
    2. Then execute:
        a. docker volume create beta-data
        b. docker-compose build
        c. docker-compose up
        note: When running docker-compose up (if on macOS), there will be a warning regarding the OS environment variable missing. This can be safely ignored
7. View the app by visiting http://localhost:3000/
8. Use Insomnia to test HTTP methods by refering to the CRUD Documentation below.



## API functions, methods, ports and URLs

| API | Function | Method | Port | URL |
| --- | --- | --- | --- | --- |
| Sales | Get a list of all AutomobileVOs | GET | 8090 | http://localhost:8090/api/automobiles/ |
| Sales | Create a new customer | POST | 8090 | http://localhost:8090/api/customers/ |
| Sales | Get a list of all customers | GET | 8090 | http://localhost:8090/api/customers/ |
| Sales | Update a specific customer | PUT | 8090 | http://localhost:8090/api/customers/:id/ |
| Sales | Delete a specific customer | DELET | 8090 | http://localhost:8090/api/customers/:id/ |
| Sales | Create a new salesperson | POST | 8090 | http://localhost:8090/api/salespeople/ |
| Sales | Get a list of all salespeople | POST | 8090 | http://localhost:8090/api/salespeople/ |
| Sales | Update a specific salesperson | PUT | 8090 | http://localhost:8090/api/salespeople/:id/ |
| Sales | Delete a specific salesperson | DELETE | 8090 | http://localhost:8090/api/salesteam/:id/ |
| Sales | Create a new sale | POST | 8090 | http://localhost:8090/api/salespeople/ |
| Sales | Get a list of all sales | POST | 8090 | http://localhost:8090/api/salespeople/ |
| Service | Get a list of all Auto technicians | GET | 8080 | http://localhost:8080/api/technicians/ |
| Service | Get details of a specific Auto technician | GET | 8080 | http://localhost:8080/api/technicians/:id/ |
| Service | Create a new Auto technician | POST | 8080 | http://localhost:8080/api/technicians/ |
| Service | List Service appointments | GET | 8080 | http://localhost:8080/api/appointments/ |
| Service | Get details of a specific Service appointment | GET | 8080 | http://localhost:8080/api/appointments/:id/ |
| Service | Create a Service appointment | POST | 8080 | http://localhost:8080/api/appointments/ |
| Inventory | Create a new manufacturer | POST | 8100 | http://localhost:8100/api/manufacturers/ |
| Inventory | Get a list of all manufacturers | GET | 8100 | http://localhost:8100/api/manufacturers/ |
| Inventory | Update a specific manufacturer | PUT | 8100 | http://localhost:8100/api/manufacturers/:id/ |
| Inventory | Delete a specific manufacturer | DELETE | 8100 | http://localhost:8100/api/manufacturers/:id/ |
| Inventory | Create a new vehicle model | POST | 8100 | http://localhost:8100/api/models/ |
| Inventory | Get a list of all models | GET | 8100 | http://localhost:8100/api/models/ |
| Inventory | Update a specific model | PUT | 8100 | http://localhost:8100/api/models/:id/ |
| Inventory | Update a specific model | DELETE | 8100 | http://localhost:8100/api/models/:id/ |
| Inventory | Create a new automobile | GET | 8100 | http://localhost:8100/api/automobiles/ |
| Inventory | Get a list of all automobiles | GET | 8100 | http://localhost:8100/api/automobiles/ |
| Inventory | Update a specific automobile | PUT | 8100 | http://localhost:8100/api/automobiles/:vin/ |
| Inventory | Delete a specific automobile | DELETE | 8100 | http://localhost:8100/api/automobiles/:vin/ |




## Services Microservice
The Service Microservice enables users to:

Create a technician
- View the list of potential technicians to perform a service appointment
- Create a new appointment
- View the list of upcoming appointments (in VIP and non-VIP table sections)
- Perform cancel and finished operations on each appointment
- Search for appointments pertaining to a specific VIN and view information specific to VIN (unfinished, finished, etc.)

The models in the Service Microservice include:

- AutomobileVO Model: Representation of vehicles in inventory and obtaining all data concerning each attribute: import_href, color, year, and vin that are imported from the inventory API.
- Technician Model: Representation of Technician with attributes of name_first, name_last and employee_number.
- Appointment Model: Representation of Appointment with attributes of vin, customer_name, vip (boolean), date, time, reason, is_finished (boolean) and technician (ForeignKey).

The Service Microservice obtains data regarding automobiles via the poller from the inventory API in order to create VO objects. This information allows for assigning of a VIP status: either VIP (true) if the appointment created has an associated VIN that matches an automobile VIN in the Inventory or VIP (false) if the appointment created has no associated VIN.

### Cruds for each API:
#### List Appointments (GET)
* **Response**
```
    {
        "id": 24,
        "vin": "12345-1",
        "customer_name": "Cust-22",
        "vip": false,
        "date": "2023-04-04",
        "time": "06:00:00",
        "reason": "Tire",
        "is_finished": false,
        "technician": {
            "id": 3,
            "name_first": "Bran-1",
            "name_last": "Tai-2",
            "employee_number": "101"
    }
```

#### Add an Appointment (POST)
* **Request**
```
    {
        "vin": "12345-8",
        "customer_name": "Cust-29",
        "vip": false,
        "date": "2023-04-04",
        "time": "06:00:00",
        "reason": "Tire",
        "is_finished": false,
        "technician": 3
    }
```
* **Response**
```
    {
        "id": 34,
        "vin": "12345-8",
        "customer_name": "Cust-29",
        "vip": false,
        "date": "2023-04-04",
        "time": "06:00:00",
        "reason": "Tire",
        "is_finished": false,
        "technician": {
            "id": 3,
            "name_first": "Bran-1",
            "name_last": "Tai-2",
            "employee_number": "101"
        }
    }
```

#### Update a specific Appointment (POST)
* **Request**
```
    {
        "vin": "12345-1",
        "customer_name": "Cust-3",
        "vip": false,
        "date": "2023-04-04",
        "time": "06:00:00",
        "reason": "Tire",
        "is_finished": false,
        "technician": 3
    }
```
* **Response**
```
    {
        "id": 7,
        "vin": "12345-1",
        "customer_name": "Cust-3",
        "vip": false,
        "date": "2023-04-04",
        "time": "06:00:00",
        "reason": "Tire",
        "is_finished": false,
        "technician": {
            "id": 3,
            "name_first": "Bran-1",
            "name_last": "Tai-2",
            "employee_number": "101"
        }
    }
```

#### Get detail of a specific Appointment (PUT)
* **Response**
```
    {
        "id": 10,
        "vin": "12345-1",
        "customer_name": "Cust-6",
        "vip": false,
        "date": "2023-04-04",
        "time": "06:00:00",
        "reason": "Tire",
        "is_finished": true,
        "technician": {
            "id": 3,
            "name_first": "Bran-1",
            "name_last": "Tai-2",
            "employee_number": "101"
        }
    }
```

#### Delete a specific Appointment (DELETE)
* **Response**
```
    {
        "deleted": true
    }
```



### Inventory
#### List Manufacturers (GET)
* **Response**
```
    {
        "href": "/api/manufacturers/2/",
        "id": 2,
        "name": "Doge"
    }
```

#### Add a Manufacturer (POST)
* **Request**
```
    {
    "name": "Toyota"
    }
```
* **Response**
```
    {
        "href": "/api/manufacturers/3/",
        "id": 3,
        "name": "Toyota"
    }
```

#### Update a specific Manufacturer (PUT)
* **Request**
```
    {
    "name": "Honda"
    }
```
* **Response**
```
    {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Honda"
    }
```

#### Get detail of a specific Manufacturer (GET)
* **Response**
```
    {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Chrysler"
    }
```

#### Delete a specific Manufacturer (DELETE)
* **Response**
```
    {
        "id": null,
        "name": "Doge"
    }
```

#### List Automobiles (GET)
* **Response**
```
    {
        "href": "/api/automobiles/12345-1/",
        "id": 1,
        "color": "White",
        "year": 2022,
        "vin": "12345-1",
        "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Camry",
            "picture_url": "https://toyota-cms-media.s3.amazonaws.com/wp-content/uploads/2019/04/2015_Toyota_Camry_028-1500x900.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/3/",
                "id": 3,
                "name": "Toyota"
            }
        },
        "sold": false
    },
```

#### Add a Automobile (POST)
* **Request**
```
    {
    "color": "red",
    "year": 2012,
    "vin": "1C3CC5FB2AN120174",
    "model_id": 1
    }
```
* **Response**
```
    {
        "href": "/api/automobiles/1C3CC5FB2AN120174/",
        "id": 3,
        "color": "red",
        "year": 2012,
        "vin": "1C3CC5FB2AN120174",
        "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Camry",
            "picture_url": "https://toyota-cms-media.s3.amazonaws.com/wp-content/uploads/2019/04/2015_Toyota_Camry_028-1500x900.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/3/",
                "id": 3,
                "name": "Toyota"
            }
        },
        "sold": false
    }
```

#### Update a specific Automobile (PUT)
* **Request**
```
    {
    "color": "red",
    "year": 2013,
    "vin": "1C3CC5FB2AN120174",
    "model_id": 1
    }
```
* **Response**
```
    {
        "href": "/api/automobiles/1C3CC5FB2AN120174/",
        "id": 3,
        "color": "red",
        "year": 2013,
        "vin": "1C3CC5FB2AN120174",
        "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Camry",
            "picture_url": "https://toyota-cms-media.s3.amazonaws.com/wp-content/uploads/2019/04/2015_Toyota_Camry_028-1500x900.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/3/",
                "id": 3,
                "name": "Toyota"
            }
        },
        "sold": false
    }
```

#### Get detail of a specific Automobile (GET)
* **Response**
```
    {
        "href": "/api/automobiles/12345-2/",
        "id": 2,
        "color": "Blue",
        "year": 2021,
        "vin": "12345-2",
        "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Camry",
            "picture_url": "https://toyota-cms-media.s3.amazonaws.com/wp-content/uploads/2019/04/2015_Toyota_Camry_028-1500x900.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/3/",
                "id": 3,
                "name": "Toyota"
            }
        },
        "sold": false
    }
```

#### Delete a specific Automobile (DELETE)
* **Response**
```
    {
        "href": "/api/automobiles/12345-2/",
        "id": null,
        "color": "Blue",
        "year": 2021,
        "vin": "12345-2",
        "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Camry",
            "picture_url": "https://toyota-cms-media.s3.amazonaws.com/wp-content/uploads/2019/04/2015_Toyota_Camry_028-1500x900.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/3/",
                "id": 3,
                "name": "Toyota"
            }
        },
        "sold": false
    }
```
# SALES!!! WHOOOOOOHOOOO

The sales microservice keeps track of each Sales person, customer, and recorded sales. I used 4 models for the service:
1. AutomobileVO- I used this value object to create a poller and grab the vin numbers from the inventory microservice.

2. SalesPerson- the properties for this model are first_name, last_name, employee_number

3. Customer- the properties for this model are first_name, last_name, address, and phone_number

4. Sale- this model contains 3 foreign keys: customer, sales_person, and automobile and one input for the price of the automobile.

## Cruds for each API:

### Customer Cruds:

```
GET:http://localhost:8090/api/customers/
POST:http://localhost:8090/api/customers/
DELETE:http://localhost:8090/api/customers/<int:id>/

JSON to Create a customer:
{
	"first_name": "test5",
	"last_name":"test5",
	"address":"123 Lane Dr",
	"phone_number":12
}
Response:
{
	"first_name": "test5",
	"last_name": "test5",
	"address": "123 Lane Dr",
	"phone_number": 12,
	"id": 8
}
```



### Sales Person Crud
```
GET: http://localhost:8090/api/salespeople/
POST: http://localhost:8090/api/salespeople/
DELETE: http://localhost:8090/api/salespeople/<int:id>/

JSON to create a Sales Person:
{
	"first_name": "test5",
	"last_name":"test5",
	"employee_number":123
}

Response:
{
	"first_name": "test5",
	"last_name": "test5",
	"employee_number": 123,
	"id": 8
}
```

### Sale Record Crud
```
GET: http://localhost:8090/api/sales/
POST: http://localhost:8090/api/sales/
DELETE:	http://localhost:8090/api/sales/<int:id>/

JSON to create a Sales Record:
{
	"price":690,
	"sales_person": 3,
	"customer":2,
	"automobile": "hm will it work"
}

Response:
{
	"price": 690,
	"sales_person": {
		"first_name": "test5",
		"last_name": "test5",
		"employee_number": 123,
		"id": 3
	},
	"customer": {
		"first_name": "Robert",
		"last_name": "V",
		"address": "123 Lane Dr",
		"phone_number": "12345678",
		"id": 2
	},
	"automobile": {
		"vin": "hm will it work",
		"import_href": "/api/automobiles/hm%20will%20it%20work/",
		"id": 11
	},
	"id": 9
}
```
